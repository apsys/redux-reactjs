# Introducción 
ReactJS es una de las tecnologías web que ha tomado una mayor popularidad debido a sus capacidades, sin embargo comenzar a usarlo en un proyecto no es un camino obvio ni trivial, debido en gran parte a la cantidad de herramientas y tecnologías que acompañan a ReactJS, haciendo un ecosistema muy completo y complejo en el cual es muy sencillo perderso, sobre todo para la gente que va empezando a usar estas herramientas (Webpack/Babel/Brunch/NPM/Node/React).

En este documento usaremos algunas herramientas que pretenden simplificar este proceso de creación e inicialización de un proyecto base _zero_ explicando los conceptos básicos necesarios para usar __ReactJS__, __Redux__, __React-Route__, __IdentityServer__ entre otras

El material que hemos creado esta basado en videos, blogs y muchas aportaciones de la comunidad que hemos encontrado valiosa. Intentaremos dejar lo mejor documentado la fuente de información original para dar crédito a sus autores originales y tambien para futuras aclaraciones sobre interpretaciones incorrectas que hayamos hecho de manera involuntaria.

Uno de las fuentes más valiosas que encontramos fue una serie de videos de LearnCode.academy, y es el que propociona el temario principal de esta taller

LearnCode.academy channel: https://www.youtube.com/channel/UCVTlvUkGslCV_h-nSAId8Sw

## Create React App
https://github.com/facebookincubator/create-react-app

__Create React App__ es un herramienta creada y mantenida de manera oficial por el equipo de React para crear aplicaciones _Single Page_. Usaremos esta herramienta para simplificar y acelerar el proceso de configuración de nuestro ambiente de desarrollo

### Instalación
Primero, debemos instalar _create-react-app_ de manera global

```
    npm install -g create-react-app
```

Una vez instalado de forma global, deberemos inicializar en una carpeta vacia. Para este ejemplo crearemos una carpeta en `C:\workshops\reactjs-workshops`

```
    cd workshops
    create-react-app reactjs-workshops
    cd reactjs-workshops
    npm start
```

Una vez ejecutada la última línea, se abrirá un navegador con una versión inicial de la aplicación

# Entendiendo Redux
http://redux.js.org/

https://egghead.io/courses/getting-started-with-redux

https://www.youtube.com/watch?v=ucd5x3Ka3gw&t=3s

Redux es un framework nos permite mantener el estado de una aplicación Javascript de manera que se comporte de manera consistente. 
Aunque está vinculada fuertemente con ReactJS, es completamente independiente y es importante entender sus principios y filosofía pasa saber como encaja en todo el ecosistema.

Empezaremos a entender los conceptos de Redux siguiendo los siguientes pasos

## Instalación 
Instalaremos Redux con la siguiente linea 

```
    npm install redux --save-dev
```

Una vez instalado Redux, abriremos el archivo _src/index.js_ sustituyendo el contenido por el siguiente código

```javascript
import { createStore} from 'redux';

const reducer = function(state, action){
    return "Hello Redux"
}

const store = createStore(reducer, 0);

store.subscribe(() => {
    console.log("Store changed", store.getState())
})

store.dispatch({type:'INC', payload: 1})

```

Abre las herramientas de desarrollador en tu navegados y verifica el mensaje enviado a la consola `Store changed Hello Redux`

En este código veremos tres conceptos básicos de Redux: __Store__, __Action__ y __Reducer__

## Store
http://redux.js.org/docs/basics/Store.html

Store es un objeto que tiene las siguientes responsabilidades dentro de nuestra aplicación

* Mantiene todo el estado de la aplicación;
* Permite el acceso al estado a través del método getState();
* Permite que el estado sea actualizado a través del método `dispatch(action)`;
* Registra a los suscriptores de las _acciones_ a través del método `subscribe(listener)`;

Es importante entender en Redux solo existe una Store dentro de toda la aplicación y es su única fuente confiable de información

## Actions
http://redux.js.org/docs/basics/Actions.html

Son objetos JavaScript que son enviados a a la Store a través del método `dispatch()`. Las Actions son la única forma de actualizar el estado de la Store

```
{
  type: 'ADD_TODO',
  text: 'Build my first Redux app'
}
```

Un Action debe tener un propiedad `type` que indique el tipo de acción que debe ser ejecutada, definidas normalmente como _strings_. En caso de que no exista esta propiedad, no podrá ser reconocida como una _Action_


## Reducers
http://redux.js.org/docs/basics/Reducers.html

Las _Actions_ describen el hecho de que algo pasó dentro de la aplicación, pero no especifican como debe cambiar el estado de la aplicación en respuesta. Esta es la responsabilidad de los _Reducers_. Un Reducer es una function _pura_ que toma el estado previo de la aplicación y una acción, y regresa el nueveo estado de la aplicación

```javascript
const reducer = function(state, action){
}
```

__Importate!!__ Es muy importante que los Reducer sean funciones puras, es decir, _dados los mismos argumentos, deben calcular el siguiente estado de la aplicación y regresarlo. Sin sorpresas, sin efectos colaterales, sin llamadas a APIs, sin mutaciones. Solo calcular el siguiente estado_
https://egghead.io/lessons/javascript-redux-pure-and-impure-functions

Cambiemos la implementación original de la siguiente manera: 

```javascript
...
const reducer = function(state, action){
    if (action.type === "INC") { 
        return state + action.payload ;
    }
    else if (action.type === "DEC") { 
        return state - action.payload ;
    }
    return state;
}
...

store.dispatch({type:'INC', payload: 1})
store.dispatch({type:'INC', payload: 5})
store.dispatch({type:'DEC', payload: 3})

```
Verifiquemos el mensaje en nuestra consola del navegador. Debes ver los suguientes mensajes:


> Store changed 1
> Store changed 6
> Store changed 3

